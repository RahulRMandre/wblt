﻿$(document).ready(function () {
    $("#change_form").submit(function (e) {
        e.preventDefault();
        console.log("changepassword button clicked");
        // $(this).attr("disabled", true);
        console.log("changepassword button Disabled");
        //var emailid = window.location.search;
        //var startIndex = emailid.indexOf('=');
        //    startIndex= startIndex + 1;
        //emailid = emailid.substring(startIndex);
        var url = window.location.search;
        var startIndex = url.indexOf('?');
        startIndex = startIndex + 1;
        url = url.substr(startIndex);
        var parts = url.split("&");
        var part1 = parts[0]; //  email=blah  
        var part2 = parts[1]; // temporary=abcde
        var emailIndex = part1.indexOf('=');
        emailIndex = emailIndex + 1;
        var emailid = part1.substr(emailIndex);
        var tpasswordIndex = part2.indexOf('=');
        tpasswordIndex = tpasswordIndex + 1;
        var temporarypassword = part2.substring(tpasswordIndex);
       
        var changeFailMsg = document.getElementById("change_fail");
        var changeSuccessMsg = document.getElementById("pass_change_success");
        if (check() === false) {
            document.getElementById("confirm_password").focus();
            console.log("Password Doesnt Match");
        }
        else {
            console.log("done");
        $.post("/api/PasswordReset",
            {

                "UserEmail": emailid,
                "tempPassword": temporarypassword,
                "UserPassword": $("#password").val(),
                "PhoneNumber": $("#phonenumber").val()

            }


            ,

            function (data, status) {
                console.log(data)
            //    alert(data);
                if (status === "success") {
                    if ((data === "Password not changed") || (data === "invalid temporary password") ) {
                        console.log("failed");
                        document.getElementById("alterMsg").innerHTML = data;
                        changeFailMsg.style.display = "block";
                        changeSuccessMsg.style.display = "none";
                    }
                    else {

                        changeSuccessMsg.style.display = "block";
                        changeFailMsg.style.display = "none";
                        setTimeout(function () {
                            location.href = "/login.html"
                        }, 5000);
                        location.href = "/login.html"
                        
                    }  }
            });//end post method
    }  });//click
});//doc ready
