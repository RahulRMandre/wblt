﻿$(document).ready(function () {
    $("#restPassEmail_form").submit(function (e) {
        e.preventDefault();
        console.log("forgotpassword button clicked");
       // $(this).attr("disabled", true);
        console.log("forgotpassword button Disabled");

        var changeFailMsg = document.getElementById("change_fail");
        var changeSuccessMsg = document.getElementById("pass_change_success");
        $.post("/api/ForgetPassword",
            {

                "UserEmail": $("#email").val(),
                 "UserPassword": $("#password").val(),
                //"PhoneNumber": $("#phonenumber").val(),

            }


            ,

            function (data, status) {
                console.log(data)
              //  alert(data);
                if (status === "success") {
                    if (data === "User not exist") {
                        console.log("failed");
                        document.getElementById("alterMsg").innerHTML = data;
                        changeFailMsg.style.display = "block";
                        changeSuccessMsg.style.display = "none";
                    }

                    else {
                        document.getElementById("disData").innerHTML = data;
                        changeSuccessMsg.style.display = "block";
                        changeFailMsg.style.display = "none";
                        setTimeout(function () {
                            location.href = "/index.html"
                        }, 5000);
                        

                    }
                }
            });
    });
    $(this).attr("disabled", true);//doc ready
});
