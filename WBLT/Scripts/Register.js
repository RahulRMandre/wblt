﻿
//checks whether the user already existed while entering email-id in the registration page
//function is called when the email-id text box loses focus

function check_ExistingUser() {
    if (document.getElementById('email').value.length >= 1)
    {
        //check in the browser whether the control entered to function
        console.log("Blured");
        var blurRedMsg = document.getElementById("blurResult_red");
        var BlurGreenMsg = document.getElementById("blurResult_green");
        //sending the post request to the RegisterController with useremail as data
        $.post("/api/Register",
            {

                "UserEmail": $("#email").val()

            }

            ,
            //data is the ruturn value from the controller and status is the result of the post operation
            function (data, status) {
                console.log(data);
                if (status === "success") {
                    //  alert(data);
                    if (data === "Available") {
                        document.getElementById("blurResult_green").innerHTML = data;
                        BlurGreenMsg.style.display = "inline";
                        blurRedMsg.style.display = "none";
                    } else {
                        document.getElementById("blurResult_red").innerHTML = data;
                        blurRedMsg.style.display = "inline";
                        BlurGreenMsg.style.display = "none";
                    }


                }
                else {
                    console.log("failed");
                    alert(failed);
                }
            });
    }
}

//the user provided values are sent to the post method to the RegisterController when the register button is clicked
$(document).ready(function () {
    $("#reg_form").submit(function (e) {
        e.preventDefault();
        console.log("Register button clicked");
        var regSuccessMsg = document.getElementById("reg_success");
        var regFailMsg = document.getElementById("regfail");
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        var msg = document.getElementById("validmessage");
        var pmsg = document.getElementById("validmessagePhone");
        var email = document.getElementById("email").value;
        if (reg.test(email) === false) {
            msg.style.display = "inline";
            document.getElementById("email").focus();
            console.log("invaild email");
            //  alert("hello3");
        }
        else if (pnumber() === false) {
            pmsg.style.display = "inline";
            document.getElementById("phonenumber").focus();
            console.log("invaild phoneNumber");
        }
        else if (check() === false) {
            document.getElementById("confirm_password").focus();
            console.log("Password Doesnt Match");
            //alert("hello1");
        }
        else {
            //The parameters sent to RegisterController
            console.log("done");
            $.post("/api/Register",
                {

                    "UserEmail": $("#email").val().toLowerCase(),
                    "UserPassword": $("#password").val(),
                    "PhoneNumber": $("#phonenumber").val()

                }
                ,
                //data is the ruturn value from the controller and status is the result of the post operation 
                function (data, status) {
                    console.log(data);
                    if (status === "success") {
                        regSuccessMsg.style.display = "block";
                        //on successful registration directed to the login page
                        location.href = "/login.html";
                    }
                    else {
                        console.log("failed");
                        regFailMsg.style.display = "block";
                        // $(#reg_form).reset();
                    }
                });
            //prevent multiple post request
            $(this).attr("disabled", true);
        }
    });
});


