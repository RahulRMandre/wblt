$(document).ready(function () {
    //Validate email on blur 
    $("#email").blur(function ()
    {
        // pattern matching email id
        var em = document.getElementById("email").value;
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        var msg = document.getElementById("validmessage");

        // pattern matching false
       if (reg.test(em) === false)
        {
            msg.style.display = "inline";
            //   alert('Invalid Email Address');
            //  document.getElementById("email").focus();
            return false;
        }
        // pattern matching false end

        // pattern matching true
        else
        {
            msg.style.display = "none";
            return true;
        }
        // pattern matching true end
    });
    //Validate email on blur end

    //form submit
    $("#form").submit(function (e)
    {
        e.preventDefault();
        //alert("button clicked");
        console.log("form submitted");
        var loginSuccessMsg = document.getElementById("login_success");
        var invailUserMsg = document.getElementById("invailUserMsg");

        var em = document.getElementById("email").value;
        var pa = document.getElementById("password").value;
        //console.log(pa)

        //validate front end
        //both null
        if ((em || pa) === "")
        {
            console.log("both null")

            loginSuccessMsg.style.display = "none";
        }
        //both null end

        //email end
        else if ($("#email").val() === "")
        {
            console.log("email null")
            document.getElementById("email").focus();

            loginSuccessMsg.style.display = "none";
        }
        //email null end

        //password null 
        else if ($("#password").val() === "")
        {
              console.log("password null")
              document.getElementById("password").focus();

              loginSuccessMsg.style.display = "none";
        }
        //password null end

        //Validation Starts
        else //Validation ok 
        {
            var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            var msg = document.getElementById("validmessage");

               if (reg.test(em) === false)
               {
                   msg.style.display = "inline";
               }//pattern matching fail

               else//pattern matching true
               {
                //    emptyTextBoxMsg.style.display = "none";
                //validatiob sucessful call api for login
                   $.post("/api/login",
                       {
                           "UserEmail": $("#email").val().toLowerCase(),
                           "UserPassword": $("#password").val(),
                       }


                       ,

                       function (data, status) {
                           // console.log(data)
                           // alert(data);
                           if (data === "PASS")//username password correct
                           {
                               loginSuccessMsg.style.display = "block";
                               invailUserMsg.style.display = "none";


                               //.style.display = "block";
                               console.log("Login is succesfull an otp will be sent to your registered device");
                               //send otp
                               $.get("/api/Otp" + '?email=' + $("#email").val())
                                   .done(function (data) {
                                       console.log(data);
                                       //console.log("otp has been sent to your registered device"); 
                                       location.href = "/otp.html?email=" + $("#email").val();
                                   })
                                   .fail(function (jqXHR, textStatus, err) {

                                       console.log("otp sending failed Reason:" + data);
                                   });

                           }//login pass

                           else {
                               //fail
                               console.log(data);
                               invailUserMsg.style.display = "block";
                               console.log(data);
                               //invailUserMsg.style.display = "block";
                           }//login fail
                       }//function end
                    );//end post method

                }//pattern matching
    
        }
        //Validation ok end
        
    
    });
    //form submit end
});
//document  ready end