﻿

$(document).ready(function () {

    var emailid = window.location.search;
    var startIndex = emailid.indexOf('=');
    startIndex = startIndex + 1;
    emailid = emailid.substring(startIndex);
    document.getElementById("email").innerHTML = emailid;
    // alert(emailid);

    $("#otpform").submit(function (e) {
        e.preventDefault();
        console.log("otp button clicked");

        //alert(emailid);
        var otpSuccessMsg = document.getElementById("otp_success");
        var otpFailMsg = document.getElementById("otpfail");

        $.post("/api/Otp",
            {
                "email": emailid,
                "otp": $("#otp").val(),
            }
            ,

            function (data, status) {

                //alert(data);
                if (data === true) {
                    console.log("otp verified");
                    otpSuccessMsg.style.display = "block";
                    otpFailMsg.style.display = "none";
                    var url = "/terminal.html?email=" + emailid;
                    location.href = url;
                    console.log("otp verified");
                    //green color 
                }
                else {
                    console.log("otp is wrong");
                    otpSuccessMsg.style.display = "none";
                    otpFailMsg.style.display = "block";
                    //red color
                }

            }//function

        );//end post method*/           
    });//click
});//doc ready