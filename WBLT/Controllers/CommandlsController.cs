﻿/* Author       :  Rahul R Mandre , Pavan V 
 * Date Modified:  29 JULY 2018 09:25 PM
 * Description  :  This Class contains the implementation of the linux shell command ls               
 *Usage         :  ls           
*/
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WBLT.Models;

namespace WBLT.Controllers
{
    public class CommandlsController : ApiController
    {
        //ls command
        #region get method
        /*
         
        public string Get(string userEmail, string currentDirectory)
        {
            string lsoutput = ". ..";//. and .. are present in all directories
            #region sql commands
            //    SqlConnection myConnection = new SqlConnection(Data.ConnectionString);

            //    try
            //    {
            //        myConnection.Open();
            //        SqlCommand commd = new SqlCommand("Command Goes here");//ls command

            //        // "SELECT * FROM [dbo].[UserInformation] where UserEmail='" + email + "' AND UserPassword='" + password + "';");
            //        commd.Connection = myConnection;

            //        var myReader = commd.ExecuteReader();

            //        while (myReader.Read())
            //        {


            //            lsoutput += "\t" + myReader["UserEmail"].ToString();//plz provide coloumn name


            //        }
            //        myConnection.Close();
            //        return lsoutput;
            //    }
            //    catch (Exception exc) { return exc.ToString(); }
            #endregion

            #region stored procedures
            //current dir=user/Folder1/SubFolder1/Subfolder11:- bit=2
            //current dir=user/Folder1/SubFolder1            :- bit=2
            //current dir=user/Folder1/                      :- bit=1
            //current dir=user                               :- root
            //calculate bit

            //example userEmail=test@test.com currentDirectory=test@test.com/test1/tt1
            //count slashes for bit
            int bit = 1;
            int count = 0;
            foreach (char slash in currentDirectory)
            {
                if (slash == '/')
                {
                    count++;
                }
            }
            if (count >= 2) { bit = 2; }

            // call stored procedures
            SqlConnection con = new SqlConnection(Data.ConnectionString);
            SqlCommand cmd = new SqlCommand("USP_LsCommand_Test", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@userEmail", userEmail);
            cmd.Parameters.AddWithValue("@fullpath", currentDirectory);
            cmd.Parameters.AddWithValue("@bit", bit);


            try
            {
                con.Open();
            }
            catch (Exception e)
            {
                return e.ToString();
            }

            SqlDataReader rdr = cmd.ExecuteReader();


            if (bit == 1)
            {
                while (rdr.Read())
                {
                    lsoutput += "  " + rdr["SubFolderDirectory"].ToString();
                }
            }

            else
            {
                while (rdr.Read())
                {
                    lsoutput += "  " + rdr["FileName"].ToString();
                }
            }


            con.Close();
            #endregion
            return lsoutput;
        }
        */
        #endregion

        public string Post(Command command)
        {
            string lsoutput = ". ..";//. and .. are present in all directories
            #region sql commands
            //    SqlConnection myConnection = new SqlConnection(Data.ConnectionString);

            //    try
            //    {
            //        myConnection.Open();
            //        SqlCommand commd = new SqlCommand("Command Goes here");//ls command

            //        // "SELECT * FROM [dbo].[UserInformation] where UserEmail='" + email + "' AND UserPassword='" + password + "';");
            //        commd.Connection = myConnection;

            //        var myReader = commd.ExecuteReader();

            //        while (myReader.Read())
            //        {


            //            lsoutput += "\t" + myReader["UserEmail"].ToString();//plz provide coloumn name


            //        }
            //        myConnection.Close();
            //        return lsoutput;
            //    }
            //    catch (Exception exc) { return exc.ToString(); }
            #endregion

            #region stored procedures
            //current dir=user/Folder1/SubFolder1/Subfolder11:- bit=2
            //current dir=user/Folder1/SubFolder1            :- bit=2
            //current dir=user/Folder1/                      :- bit=1
            //current dir=user                               :- root
            //calculate bit

            //example userEmail=test@test.com currentDirectory=test@test.com/test1/tt1
            //count slashes for bit
            command.currentDirectory = command.userEmail + command.currentDirectory;

            //intialize
            int bit = 1;
            int count = 0;

            //compute bit
            foreach (char slash in command.currentDirectory)
            {
                if (slash == '/')
                {
                    count++;
                }
            }
            if (count >= 1) { bit = 2; }

            // call stored procedures
            SqlConnection con = new SqlConnection(Data.ConnectionString);
            SqlCommand cmd = new SqlCommand("USP_LsCommand_Test", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@userEmail", command.userEmail);
            cmd.Parameters.AddWithValue("@fullpath", command.currentDirectory);
            cmd.Parameters.AddWithValue("@bit", bit);

            //open database connection
            try
            {
                con.Open();
            }
            catch (Exception)
            {
                return "Database Exception";
            }

            //run query
            SqlDataReader rdr=null;
            try
            {
                rdr = cmd.ExecuteReader();
            }
            catch (Exception )
            {
                return "query exception";
            }   

            //get ls output
            
            if (bit == 1)
            {
                while (rdr.Read())
                {
                    lsoutput += "\n" + rdr["SubFolderDirectory"].ToString();
                }
            }

            else
            {
                while (rdr.Read())
                {
                    lsoutput += "\n" + rdr["FileName"].ToString();
                }
            }


            con.Close();
            #endregion
            return lsoutput;
        }
    }
}
