﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.Threading.Tasks;
using WBLT.Models;
using System.Data.SqlClient;
using System.Data;
using System.Security.Cryptography;

namespace WBLT.Controllers
{
    public class ForgetPasswordController : ApiController
    {
        // private static object temppassword;

        public string GenerateString(int length)
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()}{[]|?><~/";

            //int length = 8;
            string randomstring = "";
            using (System.Security.Cryptography.RNGCryptoServiceProvider provider = new RNGCryptoServiceProvider())
            {
                while (randomstring.Length != length)
                {
                    byte[] oneByte = new byte[1];
                    provider.GetBytes(oneByte);
                    char character = (char)oneByte[0];
                    if (valid.Contains(character))
                    {
                        randomstring += character;
                    }
                }
            }
            return randomstring;
        }

        public void insertTemporaryPassword(string email,string temppassword)
        {
            int temporarypassword_response;
            var connectionString = @"Data Source=dynamites.database.windows.net;Initial Catalog=LTS;Persist Security Info=True;User ID=LTS;Password=Abc@1234";
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand("USP_InsertTemporaryPassword", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@useremail", email);
            cmd.Parameters.AddWithValue("@temporarypassword", temppassword);
           
            try
            {
                con.Open();
            }
            catch (SqlException )
            {
                //Database exception
            }
            try
            {
                temporarypassword_response = cmd.ExecuteNonQuery();
            }
            catch(Exception )
            { }
        }

        public void sendmail(string tpassword, string email)
        {
            var apiKey = ForgetPassword.SendGridSecret;
            var client = new SendGridClient(apiKey);
            var from = new EmailAddress("no-reply@wblts.com", "WBLTS");
            var subject = "Temporary password for WBLTS";
            var to = new EmailAddress(email);
            var plainTextContent = "This is a plain text ";
            var htmlContent = "<strong> Click on this:  http://localhost:49776/ChangePassword.html?emailid="+email+"&temppassword="+tpassword+"   open this link and change password </strong><br/>" ;
            //var htmlContent = "<strong>Temporary Password:- </strong>" + tpassword;
            var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
            var response = client.SendEmailAsync(msg);
        }


        public string Post(UserInformation userinformation)
        {
         bool userexist = RegisterController.checkExistingUser(userinformation.UserEmail);
  
            if (userexist)
            {
                userinformation.tempPassword = GenerateString(8);
                insertTemporaryPassword(userinformation.UserEmail,userinformation.tempPassword);
                //string temporarypassword = getTemporaryPassword(userinformation.UserEmail);
                sendmail(userinformation.tempPassword, userinformation.UserEmail);
                    return "Temporary password created and sent via email please use it to reset your password";
            }
            else
                return "User not exist";

        }
    }
}