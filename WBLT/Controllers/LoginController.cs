﻿/* Author        :  Rahul R Mandre , Pavan V 
 * Date Modified :  29 JULY 2018 11:38 PM
 * Description   :  This Class contains the implementation of the login controllfer              
 * Usage         :  Used to verify users
 * 
 * 
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WBLT.Models;
using System.Data.SqlClient;

namespace WBLT.Controllers
{
    public class LoginController : ApiController
    {
        // GET: api/Login
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Login/5
        public string Get(int id)
        {
            return "value";
        }
        #region Get Method (not used)
        public IHttpActionResult Get(string email,string password)
        {
            SqlConnection myConnection = new SqlConnection(Data.ConnectionString);

            try
            {
                myConnection.Open();
                SqlCommand commd = new SqlCommand("SELECT * FROM [dbo].[UserInformation] where UserEmail='" + email +"' AND UserPassword='"+password+"';");
                commd.Connection = myConnection;

                var myReader=commd.ExecuteReader();
               
              

               
                string tempEmail=null;
                string tempPassword=null;

                while (myReader.Read())
                {


                    tempEmail = myReader["UserEmail"].ToString();
                    tempPassword = myReader["UserPassword"].ToString();

                }
                myConnection.Close();

                if (email == tempEmail && password == tempPassword)
                {
                    string msg = "PASS";
                    return Ok(msg);

                }

                else
                {
                    string msg = "FAIL";
                    return BadRequest(msg);
                }
            }
            catch (Exception exp)
            {
                //Console.WriteLine(exp.ToString());
                string msg = "EXCEPTION"+ exp.ToString();
                return BadRequest(msg);
            }


            
        }
        #endregion

        // POST: api/Login
        //public void Post([FromBody]string value)
        //{

        //}

        public IHttpActionResult Post(UserInformation userInformation)
        {
            //hash password
            userInformation.UserPassword = Data.hash(userInformation.UserPassword,userInformation.UserEmail);

            SqlConnection myConnection = new SqlConnection(Data.ConnectionString);

            try
            {
                myConnection.Open();
                SqlCommand commd = new SqlCommand("SELECT UserEmail,UserPassword FROM [dbo].[UserInformation] where UserEmail='" + userInformation.UserEmail + "' AND UserPassword='" + userInformation.UserPassword + "';");
                commd.Connection = myConnection;

                var myReader = commd.ExecuteReader();

                string tempEmail = null;
                string tempPassword = null;
                
                //read data stored in database
                while (myReader.Read())
                {
                    tempEmail = myReader["UserEmail"].ToString();
                    tempPassword = myReader["UserPassword"].ToString();
                }
                myConnection.Close();

                if (userInformation.UserEmail == tempEmail && userInformation.UserPassword == tempPassword)
                {
                    string msg = "PASS";
                    return Ok(msg);

                }
               
                else
                {
                    string msg = "Invalid Username or Password";
                    return Ok(msg);
                }
            }//end of try
            catch (Exception)
            {
                //Console.WriteLine(exp.ToString());
                string msg = "DATABASE EXCEPTION";
                return Ok(msg);
            }

        }

        // PUT: api/Login/5
        public void Put(int id, [FromBody]string value)
        {

        }

        // DELETE: api/Login/5
        public void Delete(int id)
        {
        }
    }
}
