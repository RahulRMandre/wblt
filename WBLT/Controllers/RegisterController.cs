﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http;
using WBLT.Models;

namespace WBLT.Controllers
{
    public class RegisterController : ApiController
    {
        public static string hash(string password, string email)
        {
            password = "WBLT" + password + email;
            byte[] bytes = Encoding.Unicode.GetBytes(password);
            SHA256Managed hashstring = new SHA256Managed();
            byte[] hash = hashstring.ComputeHash(bytes);
            string hashString = string.Empty;
            foreach (byte x in hash)
            {
                hashString += String.Format("{0:x2}", x);
            }
            return hashString;
        }

        public static bool checkExistingUser(string UserEmail)
        {
            string SQLEmail = null;
            SqlConnection con = new SqlConnection("Data Source=dynamites.database.windows.net;Initial Catalog=LTS;Persist Security Info=True;User ID=lts;Password=Abc@1234");
            SqlCommand cmd = new SqlCommand("USP_CheckExistingUser", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@useremail", UserEmail);

            try
            {
                con.Open();
            }
            catch (SqlException)
            {
                return false;
                //*******************************
            }
            SqlDataReader rdr;
            try { rdr = cmd.ExecuteReader(); }
            catch (Exception)
            {
                return false;//UserEmail==null
            }

            while (rdr.Read())
            {

                SQLEmail = rdr["UserEmail"].ToString();


            }
            con.Close();
            if (SQLEmail != null)
            {
                return true;
            }
            else
            {
                return false;
            }


        }



        // POST: api/Register
        public string Post(UserInformation userinformation)
        //public void Post(string userName,string password,string phone)
        {
            if (userinformation.UserEmail != null)
            {
                bool existinguser = checkExistingUser(userinformation.UserEmail);
                string post_response;
                if (existinguser)
                {
                    post_response = userinformation.UserEmail + " has already registered";
                    return post_response;
                }
                else if (userinformation.UserPassword == null)
                    return "Available";

                //DateTime dateTime = DateTime.UtcNow.Date;
                //var date= dateTime.ToString("yyyy/MM/dd");
                ////var date1 = "2018-08-08";
                ////int Id = 7;
                //var connectionString = @"Data Source=dynamites.database.windows.net;Initial Catalog=LTS;Persist Security Info=True;User ID=LTS;Password=Abc@1234";
                //SqlConnection Con = new SqlConnection(connectionString);
                //try
                //{
                //    Con.Open();
                //}
                //catch (SqlException e) {
                //    string msg = "Cannot connect to database";
                //    post_response = msg + "  " + e;
                //    return post_response;

                //}
                //var sqlCommandPreparation = @"INSERT INTO UserInformation( UserEmail,UserPassword,UserPhoneNumber,UserDateOfRegistratiom) VALUES('"+ userinformation.UserEmail + "','" +
                //    userinformation.UserPassword + "','" + userinformation.PhoneNumber + "','" + date + "');";

                //SqlCommand sqlCommand = new SqlCommand(sqlCommandPreparation);
                //sqlCommand.Connection = Con;
                //var response = sqlCommand.ExecuteNonQuery();
                //Con.Close();

                //if (response == 1)
                //{
                //    post_response = userinformation.UserEmail + " has registered Successfully";
                //}
                //else
                //     post_response = "Not  Registered";
                //return post_response;

                var hashed_password = Data.hash(userinformation.UserPassword, userinformation.UserEmail);


                var connectionString = @"Data Source=dynamites.database.windows.net;Initial Catalog=LTS;Persist Security Info=True;User ID=LTS;Password=Abc@1234";
                SqlConnection con = new SqlConnection(connectionString);
                SqlCommand cmd = new SqlCommand("USP_InsertUser_Test", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@useremail", userinformation.UserEmail);
                cmd.Parameters.AddWithValue("@userpassword", hashed_password);
                cmd.Parameters.AddWithValue("@userphone", userinformation.PhoneNumber);
                // cmd.Parameters.AddWithValue("@user", userinformation.tempPassword);
                try
                {
                    con.Open();
                }
                catch (SqlException e)
                {
                    string msg = "Cannot connect to database";
                    post_response = msg + "  " + e;
                    return post_response;
                }

                var response = cmd.ExecuteNonQuery();
                con.Close();

                if (response != 0)
                {
                    post_response = userinformation.UserEmail + " has registered Successfully";
                }
                else
                {
                    post_response = "Not  Registered";
                }
                // ForgetPasswordController.Execute();
                return post_response;

            }
            else
                return "Enter UserEmail";
            //public void Postt(string userName, string password, string phone)
            //{

            //}

        }
    }
}

           

            //simple POST. 


            //public string Test1(UserInformation testUserInformation)
            //{
            //    return "response done";
            //}
