﻿/* Author        :  Rahul R Mandre , Pavan V 
 * Date Modified :  29 JULY 2018 09:45 PM
 * Description   :  This Class contains the implementation of the linux shell command mkdir              
 * Usage         :  rmdir newDirName
 * 
 * Warning       : new directory should not contain any relative path, go to the required dir and then create the new dir 
 *  pass: rmdir existingDirName
 *  fail: rmdir dir1/existingDirName
 * 
*/
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WBLT.Models;

namespace WBLT.Controllers
{
    public class CommandrmdirController : ApiController
    {

        public bool Post(Command command)
        {
            bool rmdirOutput = false;
            bool dirExists = false;

            //check for slashes in  parameter1
            
            int dirslashindex = command.parameter1.IndexOf("/");
            if (dirslashindex!=-1)
            {
                rmdirOutput = false;
                return rmdirOutput;
            }
            

            //check if dir exists
               #region dir Exists?

            //calculate subfolder
            string subfolder = command.currentDirectory;

            if (subfolder != null)

            {   //take only subfolder name ex if cd= /dir/suDir1/susbubDir op=dir 

                int slashindex = subfolder.IndexOf("/");
                subfolder = subfolder.Substring(slashindex + 1, subfolder.Length - slashindex - 1);//remove first slash
                slashindex = subfolder.IndexOf("/");
                if (slashindex > 0)
                {
                    subfolder = subfolder.Substring(0, slashindex);//store data upto second slash
                }
            }

            //string sub = input.Substring(0, 3);
            command.currentDirectory = command.userEmail + command.currentDirectory;
            
            //intialize
            int bit = 1;
            int count = 0;

            //calculate bit  
            foreach (char charSlash in command.currentDirectory)
            {
                if (charSlash == '/')
                {
                    count++;
                }
            }
            if (count >= 1) { bit = 2; }

            //SP to check dir exists
            SqlConnection con = new SqlConnection(Data.ConnectionString);
            SqlCommand cmd = new SqlCommand("USP_CheckExistingFolderFile", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@userEmail", command.userEmail);
            cmd.Parameters.AddWithValue("@fullpath", command.currentDirectory);
            cmd.Parameters.AddWithValue("@filename", command.parameter1);
            cmd.Parameters.AddWithValue("@bit", bit);
            cmd.Parameters.AddWithValue("@filetype", "folder");


            try
            {
                con.Open();
            }
            catch (Exception)
            {
                
                return false;//Database exception
            }
            SqlDataReader rdr = null;
            try
            {
                rdr = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                return false;//Query exception
            }
           

            if (bit == 1)
            {
                while (rdr.Read())
                {
                    string data = rdr["SubFolderDirectory"].ToString();
                    if (data != null) { dirExists = true; }
                }
            }

            else
            {
                while (rdr.Read())
                {
                    string data = rdr["FileName"].ToString();
                    if (data != null) { dirExists = true; }
                }
            }
            con.Close();
            #endregion


            if (dirExists == true)
            {
                #region remove DIR
                string parentDir="";

                if (bit == 1)
                {
                    parentDir = command.parameter1;
                }

                if (bit == 2)
                {
                    //
                    parentDir = command.currentDirectory;
                    int lastSlashIndex =  parentDir.LastIndexOf('/');
                    parentDir = parentDir.Substring(lastSlashIndex + 1, parentDir.Length - lastSlashIndex - 1);//remove first slash   
                }

                con = new SqlConnection(Data.ConnectionString);
                cmd = new SqlCommand("USP_RMDIRCommandTest", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@userEmail", command.userEmail);
                //cmd.Parameters.AddWithValue("@path", command.currentDirectory);

                cmd.Parameters.AddWithValue("@filename", command.parameter1);
                cmd.Parameters.AddWithValue("@subfolder", parentDir);
                cmd.Parameters.AddWithValue("@bit", bit);



                try
                {
                    con.Open();
                }
                catch (Exception )
                {
                    return false; //database exception
                }

                rdr = cmd.ExecuteReader();
                con.Close();
                rmdirOutput = true;
                #endregion

            }
            else
            {
                rmdirOutput = false;

            }

            return rmdirOutput;
        }


    }
}
