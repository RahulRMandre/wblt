﻿/* 
 * Author        :  Rahul R Mandre , Pavan V 
 * Date Modified :  29 JULY 2018 10:00 PM
 * Description   :  This Class contains the implementation of the linux shell command mkdir              
 * Usage         :  mkfile newDirName
 * 
 * Warning       : new file should not contain any relative path, go to the required dir and 
 *                 then create the new file,file cannot be created on subfolder and can only be created in FolderAction table  
 *  pass: mkfile newFileName
 *  fail: mkfile /dir1/newFileName
 *  fail: mkfile /newFile
 * 
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WBLT.Models;

namespace WBLT.Controllers
{
    public class CommandmkfileController : ApiController
    {
        public bool Post(Command command)
        {
            bool mkfileOutput = false;
            bool fileExists = false;
        
            //check for slashes in  parameter1
            int dirslashindex = command.parameter1.IndexOf("/");

            if (dirslashindex != -1)
            {
                mkfileOutput = false;//user provided relative path
                return mkfileOutput;
            }

            //check if file exists
                #region file Exists?

            //calculate subfolder
            string subfolder = command.currentDirectory;
            if (subfolder != null)
            {
                int slashindex = subfolder.IndexOf("/");
                subfolder = subfolder.Substring(slashindex + 1, subfolder.Length - slashindex - 1);
                slashindex = subfolder.IndexOf("/");
                if (slashindex > 0)
                {
                    subfolder = subfolder.Substring(0, slashindex);
                }
            }

            
            command.currentDirectory = command.userEmail + command.currentDirectory;
            
            //intialize
            int bit = 1;
            int count = 0;
            //calculate bit
            foreach (char charSlash in command.currentDirectory)
            {
                if (charSlash == '/')
                {
                    count++;
                }
            }
            if (count >= 1) { bit = 2; }

            //false as file cannot be created in subfolder
            if (bit == 1)
            {
                return false;
            }

            //stored procedure
            SqlConnection con = new SqlConnection(Data.ConnectionString);
            SqlCommand cmd = new SqlCommand("USP_CheckExistingFolderFile", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@userEmail", command.userEmail);
            cmd.Parameters.AddWithValue("@fullpath", command.currentDirectory);
            cmd.Parameters.AddWithValue("@filename", command.parameter1);
            cmd.Parameters.AddWithValue("@bit", bit);
            cmd.Parameters.AddWithValue("@filetype", ".txt");


            try
            {
                con.Open();
            }
            catch (Exception)
            {
                return false;//database exception
            }

            SqlDataReader rdr = null;

            try
            {
                rdr = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                return false;//query exception
            }


            if(bit==2)
            {
                while (rdr.Read())
                {
                    string data = rdr["FileName"].ToString();
                    if (data != null) { fileExists = true; }
                }
            }
            con.Close();
            #endregion

            if (fileExists == false)
            {
                #region create newfile
                if (bit == 1) { subfolder = command.parameter1; }
                
                //stored procedure
                con = new SqlConnection(Data.ConnectionString);
                cmd = new SqlCommand("USP_MakeFileCommand_Test", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@useremail", command.userEmail);
                cmd.Parameters.AddWithValue("@path", command.currentDirectory);
                cmd.Parameters.AddWithValue("@filename", command.parameter1);
                cmd.Parameters.AddWithValue("@subfolder", subfolder);
                


                try
                {
                    con.Open();
                }
                catch (Exception)
                {
                    return false;//database exception
                }
                try
                {
                    rdr = cmd.ExecuteReader();
                }
                catch (Exception)
                {
                    return false;//query exception
                }
                
                con.Close();
                mkfileOutput = true;
                #endregion
            }
            else//file exists=true
            {
                mkfileOutput = false;
            }

            return mkfileOutput;
        }
    }
}
