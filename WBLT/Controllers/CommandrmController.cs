﻿/* Author        :  Rahul R Mandre , Pavan V 
 * Date Modified :  29 JULY 2018 10:38 PM
 * Description   :  This Class contains the implementation of the linux shell command mkdir              
 * Usage         :  rm oldFileName
 * 
 * Warning       :  file to be deleted  should not contain any relative path, go to the required dir and 
 *                 then delete the file,file cannot be deleted on subfolder and can only be deleted in FolderAction table  
 *  
 *  pass: rm newFileName
 *  fail: rm /dir1/oldFileName
 *  fail: rm /oldFile
 * 
*/
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WBLT.Models;

namespace WBLT.Controllers
{
    public class CommandrmController : ApiController
    {
        public bool Post(Command command)
        {
            bool rmfileOutput = false;
            bool fileExists = false;

            //check for slashes in  parameter1
            int dirslashindex = command.parameter1.IndexOf("/");

            if (dirslashindex != -1)
            {
                rmfileOutput = false;//user provided relative path
                return rmfileOutput;
            }


            //check if file exists
               #region file Exists?
            
            //calculate subfolder
            string subfolder = command.currentDirectory;

            if (subfolder != null)
            {
                int slashindex = subfolder.IndexOf("/");
                subfolder = subfolder.Substring(slashindex + 1, subfolder.Length - slashindex - 1);
                slashindex = subfolder.IndexOf("/");
                if (slashindex > 0)
                {
                    subfolder = subfolder.Substring(0, slashindex);
                }
            }

           
            command.currentDirectory = command.userEmail + command.currentDirectory;
            //intialize
            int bit = 1;
            int count = 0;

            //compute bit
            foreach (char charSlash in command.currentDirectory)
            {
                if (charSlash == '/')
                {
                    count++;
                }
            }
            if (count >= 1) { bit = 2; }

            if (bit == 1) { return false; }//cannot create file in subfolder hence cannot delete

            //call stored procedures
            SqlConnection con = new SqlConnection(Data.ConnectionString);
            SqlCommand cmd = new SqlCommand("USP_CheckExistingFolderFile", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@userEmail", command.userEmail);
            cmd.Parameters.AddWithValue("@fullpath", command.currentDirectory);
            cmd.Parameters.AddWithValue("@filename", command.parameter1);
            cmd.Parameters.AddWithValue("@bit", bit);
            cmd.Parameters.AddWithValue("@filetype", ".txt");


            try
            {
                con.Open();
            }
            catch (Exception)
            {
                return false;//database exception
            }
            SqlDataReader rdr = null;
            try
            {
                rdr = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                return false;//query exception
            }


            if (bit == 2)
            {
                while (rdr.Read())
                {
                    string data = rdr["FileName"].ToString();
                    if (data != null) { fileExists = true; }
                }
            }
            con.Close();
            #endregion

            if (fileExists == true)
            {
                #region remove file
                string parentDir;
                
              

                //get parent directory
                parentDir = command.currentDirectory;
                int lastSlashIndex = parentDir.LastIndexOf('/');
                parentDir = parentDir.Substring(lastSlashIndex + 1, parentDir.Length - lastSlashIndex - 1);//remove first slash   
                

                //stored procedures
                con = new SqlConnection(Data.ConnectionString);
                cmd = new SqlCommand("USP_RMFILECommand", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@useremail", command.userEmail);               
                cmd.Parameters.AddWithValue("@filename", command.parameter1);
                cmd.Parameters.AddWithValue("@subfolder", subfolder);
                cmd.Parameters.AddWithValue("@parentfolder", parentDir);


                try
                {
                    con.Open();
                }
                catch (Exception)
                {
                    return false;//database exception
                }
                try
                {
                    rdr = cmd.ExecuteReader();
                }
                catch (Exception)
                {
                    return false;//query exception
                }
              
                con.Close();
                rmfileOutput = true;
                #endregion
            }

            else//file exist=false
            {
                rmfileOutput = false;
            }

            return rmfileOutput;

        }
    }
}
