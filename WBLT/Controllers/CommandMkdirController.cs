﻿/* Author        :  Rahul R Mandre , Pavan V 
 * Date Modified :  29 JULY 2018 09:30 PM
 * Description   :  This Class contains the implementation of the linux shell command mkdir              
 * Usage         :  mkdir newDirName
 * 
 * Warning       : new directory should not contain any relative path, go to the required dir and then create the new dir 
 *  pass: mkdir newDirName
 *  fail: mkdir dir1/newDirName
 * 
*/
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WBLT.Models;

namespace WBLT.Controllers
{
    public class CommandmkdirController : ApiController
    {
        public bool Post(Command command)
        {
            bool mkdirOutput = false;
            bool dirExists = false;

            //check for slashes in  parameter1

            int dirslashindex = command.parameter1.IndexOf("/");

            if (dirslashindex!=-1)
            {
                mkdirOutput = false;
                return mkdirOutput;
            }
            

           
            //check if dir exists
                #region dir Exists?

            string subfolder = command.currentDirectory;
            //calculate subfolder
            if (subfolder != null)
            {
                int slashindex = subfolder.IndexOf("/");
                subfolder = subfolder.Substring(slashindex + 1, subfolder.Length - slashindex - 1);
                slashindex = subfolder.IndexOf("/");
                if (slashindex > 0)
                {
                    subfolder = subfolder.Substring(0, slashindex);
                }
            } 

            //calculate curdir
            command.currentDirectory = command.userEmail + command.currentDirectory;
            
            //initialize
            int bit = 1;
            int count = 0;
            //compute bit
            foreach (char charSlash in command.currentDirectory)
            {
                if (charSlash == '/')
                {
                    count++;
                }
            }
            if (count >= 1) { bit = 2; }

            //stored procedure
            SqlConnection con = new SqlConnection(Data.ConnectionString);
            SqlCommand cmd = new SqlCommand("USP_CheckExistingFolderFile", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@userEmail", command.userEmail);
            cmd.Parameters.AddWithValue("@fullpath", command.currentDirectory);
            cmd.Parameters.AddWithValue("@filename", command.parameter1);
            cmd.Parameters.AddWithValue("@bit", bit);
            cmd.Parameters.AddWithValue("@filetype", "folder");


            try
            {
                con.Open();
            }
            catch (Exception)
            {
                //cannot connect to database
                return false;
            }

            SqlDataReader rdr = null;
            try
            {
               rdr = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                //illegeal query
                return false;
            }
           
            //find dire exists or not
            if (bit == 1)
            {
             
                while (rdr.Read())
                {
                    string data = rdr["SubFolderDirectory"].ToString();
                    if (data != null) { dirExists = true; }
                }
            }

            else
            {
                while (rdr.Read())
                {
                    string data = rdr["FileName"].ToString();
                    if (data != null) { dirExists= true; }
                }
            }
            con.Close();
            #endregion

            if (dirExists == false)
            {
                #region create DIR
                //stored procedure
                if (bit == 1) { subfolder = command.parameter1; }
                 con = new SqlConnection(Data.ConnectionString);
                 cmd = new SqlCommand("USP_MKDIRCommand_Test", con);
                 cmd.CommandType = CommandType.StoredProcedure;
                 cmd.Parameters.AddWithValue("@userEmail", command.userEmail);
                 cmd.Parameters.AddWithValue("@path", command.currentDirectory);
                 cmd.Parameters.AddWithValue("@filename", command.parameter1);
                 cmd.Parameters.AddWithValue("@subfolder", subfolder);
                 cmd.Parameters.AddWithValue("@bit", bit);
                 


                try
                {
                    con.Open();
                }
                catch (Exception)
                {
                    return false;//database exception
                }
                try
                {
                    rdr = cmd.ExecuteReader();
                }
                catch (Exception)
                {
                    return false;//query exception
                }
               
                con.Close();
                mkdirOutput = true;
                #endregion

            }
            else//dir exists==false
            {
                mkdirOutput=false;               
            }

            return mkdirOutput;
        }
    }
}
