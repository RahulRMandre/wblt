﻿/* Author        :  Rahul R Mandre , Pavan V 
 * Date Modified :  29 JULY 2018 09:45 PM
 * Description   :  This Class contains the implementation of the linux shell command cd              
 * Usage         :  cd DirName
 * 
 * 
 *  pass: cd existingDirName
 *  pass: cd dir1/existingDirName
 *  pass: cd /dir1/dir2
 *  pass: cd ..
 *  
 * 
*/
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WBLT.Models;

namespace WBLT.Controllers
{
    public class CommandcdController : ApiController
    {
        public string Get()
        {
            return "null";
        }

        public bool Post(Command command)

        {
            bool cdoutput = false;
            #region sql commands(not used)

            //SqlConnection myConnection = new SqlConnection(Data.ConnectionString);

            //try
            //{
            //    myConnection.Open();
            //    SqlCommand commd = new SqlCommand("Command Goes here");//cd command

            //    // "SELECT * FROM [dbo].[UserInformation] where UserEmail='" + email + "' AND UserPassword='" + password + "';");
            //    commd.Connection = myConnection;


            //    var myReader = commd.ExecuteReader();
            //    var data = myReader.Read();

            //    while (myReader.Read())//if atleast 1 table exsits with the parameter name then true
            //    {
            //        cdoutput = true;
            //    }

            //    myConnection.Close();
            //   
            //}
            //catch (Exception exc) { return false; }
            #endregion


            #region Stored Procedures
            //calculate bit

            //current dir=user/Folder1/SubFolder1/Subfolder11:- bit=2
            //current dir=user/Folder1/SubFolder1            :- bit=2
            //current dir=user/Folder1                       :- bit=1
            //current dir=user                               :- root


            //example userEmail=test@test.com currentDirectory=test@test.com/test1/tt1
            //count slashes for bit

            //remove last slash
           // if (command.currentDirectory[command.currentDirectory.Length-1] == '/')
           // {
           //    command.currentDirectory=command.currentDirectory.Substring(0, command.currentDirectory.Length - 1);
           // }

            command.currentDirectory = command.userEmail  + command.currentDirectory;
            //intialize
            int bit = 1;
            int count = 0;

            //compute bit
            foreach (char charSlash in command.currentDirectory)
            {
                if (charSlash == '/')
                {
                    count++;
                }
            }
            if (count >= 2) { bit = 2; }


            int slash = command.currentDirectory.LastIndexOf('/');
            string dirName = null;
            if (slash >= 0)
            {
                int dirNameLength = command.currentDirectory.Length - slash - 1;
                 dirName = command.currentDirectory.Substring(slash + 1, dirNameLength);
                command.currentDirectory = command.currentDirectory.Substring(0, slash);
            }
           


            //run stored procedures
           
            SqlConnection con = new SqlConnection(Data.ConnectionString);
            SqlCommand cmd = new SqlCommand("USP_CDCommand_Test", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@userEmail", command.userEmail);
            cmd.Parameters.AddWithValue("@fullpath", command.currentDirectory);
            cmd.Parameters.AddWithValue("@filename", dirName);
            cmd.Parameters.AddWithValue("@bit", bit);


            try
            {
                con.Open();
            }
            catch (Exception)
            {
                return false;//database error
            }
            SqlDataReader rdr = null;
            try
            {
                rdr = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                return false;//query error
            }
            

            if (bit == 1)
            {
                while (rdr.Read())
                {
                    string data =rdr["SubFolderDirectory"].ToString();
                    if (data != null) { cdoutput = true; }
                }
            }

            else
            {
                while (rdr.Read())
                {
                    string data =rdr["FileName"].ToString();
                    if (data != null) { cdoutput = true; }
                }
            }

         
            con.Close();

            #endregion
            return cdoutput;
        }


    }
}
