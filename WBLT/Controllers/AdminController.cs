﻿/* Author        :  Rahul R Mandre 
 * Date Modified :  30 JULY 2018 12:31 AM
 * Description   :  This Class contains the implementation of the linux  command  that can only be used by the admin              
 * Usage         :  command 1 showusers
 *                  command 2 rmuser UserName
 *  
 * 
*/
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WBLT.Models;

namespace WBLT.Controllers
{
    public class AdminController : ApiController
    {
        public string Get()
        {
            string userNames = "";

            // call stored procedures
            SqlConnection con = new SqlConnection(Data.ConnectionString);
            SqlCommand cmd = new SqlCommand("USP_Admin_ViewAllUsers", con);
            cmd.CommandType = CommandType.StoredProcedure;
            


            try
            {
                con.Open();
            }
            catch (Exception e)
            {
                return e.ToString();
            }

            SqlDataReader rdr = cmd.ExecuteReader();


          
            
                while (rdr.Read())
                {
                    userNames += " " + rdr["UserEmail"].ToString();
                }
            



            con.Close();


            return userNames;
        }


        public string Post(Command command)
        {
            string rmuserOutput = null;
            bool userExists = false;

            #region Check if user exists

            SqlConnection con = new SqlConnection(Data.ConnectionString);
            SqlCommand cmd = new SqlCommand("USP_CheckExistingUser", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@useremail", command.parameter1);
            try
            {
                con.Open();
            }
            catch (Exception )
            {
                rmuserOutput = "DATABASE EXCEPTION";
                return rmuserOutput;
            }

            SqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {

                string data = rdr["UserEmail"].ToString();
                if (data != null) { userExists = true; }
               
            }

            #endregion

            if (userExists == true)
            {
                #region Delete user
                con = new SqlConnection(Data.ConnectionString);
                cmd = new SqlCommand("USP_Admin_DeleteUser", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@useremail", command.parameter1);
                try
                {
                    con.Open();
                }
                catch (Exception )
                {
                    rmuserOutput = "DATABASE EXCEPTION";
                    return rmuserOutput;
                }

                 rdr = cmd.ExecuteReader();
                rmuserOutput = "User Deleted";
                #endregion
            }

            else
            {
                rmuserOutput = "User not found";
                return rmuserOutput;
            }

            return rmuserOutput;
        }

    }
}
