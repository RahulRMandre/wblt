﻿/* Author:Rahul R Mandre 
 * Phone: 8095419955
 * Email: rahulmandre74@gmail.com
 * Date Modified: 25 JULY 2018 10:14 PM
 * 
 * IMPORTANT
 * Backdoor for otp verification line
 *  
 *  else if(otpSession.otp=="900913")
            {
                return true;
            }

            REMOVE THIS
 *
 * 
 * change url from google to other url 
 * //string url ="https://google.com"
 * //string url = " https://smsapi.engineeringtgr.com/send/?Mobile="+Data.phoneNumber[index]+"&Password="+Data.password[index]+"&Key="+Data.apiKey[index]+"&Message=OTP for login:"+otp+"&To="+phoneNumber;
            
*/


using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WBLT.Models;

namespace WBLT.Controllers
{
    public class OtpController : ApiController
    {
        // GET: api/Otp
        public string Get(int otp)
        {

            return null;   
        }

        // GET: api/Otp/email
        //generate and send otp
        public string Get(string email)
        {
            string otpStatus = "";
            string phoneNumber=null;

            #region get phone number using email from database
            SqlConnection myConnection = new SqlConnection(Data.ConnectionString);

            try
            {
                myConnection.Open();
                SqlCommand commd = new SqlCommand("SELECT * FROM [dbo].[UserInformation] where UserEmail='" + email + "';");
                commd.Connection = myConnection;

                var myReader = commd.ExecuteReader();


                while (myReader.Read())
                {
                    phoneNumber = myReader["UserPhoneNumber"].ToString();
                }
                myConnection.Close();
            }
            catch (Exception)
            {
                otpStatus = "Database Exception";
                return otpStatus;
            }
            #endregion

            #region generate otp and send 
            Random random = new Random();
            int index = random.Next(0, Data.apiKey.Length);
            phoneNumber = phoneNumber.Substring(0, 10);
            int otp = random.Next(10000, 99999);

           
            string url = "https://smsapi.engineeringtgr.com/send/?Mobile="+Data.phoneNumber[index]+"&Password="+Data.password[index]+"&Key="+Data.apiKey[index]+"&Message=OTP for login:"+otp+"&To="+phoneNumber;


            //string Rahulurl = " https://smsapi.engineeringtgr.com/send/?Mobile=8095419955&Password=sjceise&Key=rahulQf3ZPYd0q1JrHkK65gxl&Message=WebApp&To=8095419955";
            //veeresh url=https://smsapi.engineeringtgr.com/send/?Mobile=8050378093&Password=helloworld&Key=veereO7C2xdRecaNqyX6&Message=veeresh&To=8095419955
            ///string adiurl = " https://smsapi.engineeringtgr.com/send/?Mobile=8105809777&Password=A3433M&Key=nayakEwSsKDjmk92Zvd54cnF7hL&Message=Aditya&To=8105809777";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                Stream resStream = response.GetResponseStream();
                Console.WriteLine(resStream);
            }
            catch (Exception exc)
            {
                otpStatus = "API Exception" + exc.ToString();
                return otpStatus;
            }
            #endregion

            #region delete previous otp
            SqlConnection con = new SqlConnection(Data.ConnectionString);
            SqlCommand cmd = new SqlCommand("USP_DeleteOtp", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@useremail", email);

            try
            {
                con.Open();
            }
            catch (Exception)
            {
                otpStatus = "Database Exception";
                return otpStatus;
            }
            SqlDataReader rdr = null;
            try
            {
                rdr = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                otpStatus = "Query Exception";
                return otpStatus;
            }
           
            con.Close();
            #endregion

            #region store otp in database

             con = new SqlConnection(Data.ConnectionString);
             cmd = new SqlCommand("USP_InsertOtp", con);
             cmd.CommandType = CommandType.StoredProcedure;

             cmd.Parameters.AddWithValue("@otp", otp);
             cmd.Parameters.AddWithValue("@useremail", email);

            try
            {
                con.Open();
            }
            catch (Exception)
            {
                otpStatus = "Database Exception";
                return otpStatus;
            }
            try
            {
                rdr = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                otpStatus = "Query Exception";
                return otpStatus;
            }
            #endregion

            otpStatus = "otp sent to:"+phoneNumber;
            return otpStatus;
        }

        // POST: api/Otp
        //check otp
        public bool Post(OtpSession otpSession)
        {
            //get otp from db
            string DBotp = "";
            string otpStatus = "";

            SqlConnection con = new SqlConnection(Data.ConnectionString);
            SqlCommand cmd = new SqlCommand("USP_GetOtp", con);
            cmd.Parameters.AddWithValue("@useremail", otpSession.email);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                con.Open();
            }
            catch (Exception)
            {
                otpStatus = "Database Exception";
                return false;
            }

            SqlDataReader rdr = null;
            try
            {
                rdr = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                otpStatus = "Query Exception";
                return false;
            }
           
            //read data from db
            while (rdr.Read())
            {
                string data = rdr["OTP"].ToString();
                DBotp = data;               
            }

            //compare both 
            if (otpSession.otp == DBotp)
            {
                return true;
            }

            //backdoor
            else if(otpSession.otp=="900913")
            {
                return true;
            }

            else
            {
                return false;
            }
           
        }

        // PUT: api/Otp/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Otp/5
        public void Delete(int id)
        {
        }
    }
}
