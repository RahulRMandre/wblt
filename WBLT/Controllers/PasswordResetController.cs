﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Http;
using WBLT.Models;

namespace WBLT.Controllers
{
    public class PasswordResetController : ApiController 
    {

        //public string getTemporaryPassword(string email)
        //{
        //    string temporarypassword=null;
        //    SqlConnection con = new SqlConnection("Data Source=dynamites.database.windows.net;Initial Catalog=LTS;Persist Security Info=True;User ID=lts;Password=Abc@1234");
        //    SqlCommand cmd = new SqlCommand("USP_GetTemporaryPassword", con);
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    cmd.Parameters.AddWithValue("@useremail", email);

        //    try
        //    {
        //        con.Open();
        //    }
        //    catch (Exception)
        //    {
        //        return "Error in opening Database";
        //    }
        //    SqlDataReader rdr = cmd.ExecuteReader();
        //    while (rdr.Read())
        //    {
        //        temporarypassword = rdr["UserTempPassword"].ToString();
        //    }
        //    con.Close();
        //    return temporarypassword;
        //}
         

        //    public string getEmail(string temppassword)
        //{
        //    string email=null;
        //    var connectionString = @"Data Source=dynamites.database.windows.net;Initial Catalog=LTS;Persist Security Info=True;User ID=LTS;Password=Abc@1234";
        //    SqlConnection con = new SqlConnection(connectionString);
        //    SqlCommand cmd = new SqlCommand("", con);
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    try
        //    {
        //        con.Open();
        //    }
        //    catch (Exception)
        //    {

        //    }

        //    SqlDataReader rdr = cmd.ExecuteReader();

        //    while (rdr.Read())
        //    {

        //        email = rdr["UserEmail"].ToString();


        //    }
        //    con.Close();

        //    return email;

        //}
       public bool checktemporarypassword(string temppassword,string email)
        {
            string temporarypassword = null;
            SqlConnection con = new SqlConnection("Data Source=dynamites.database.windows.net;Initial Catalog=LTS;Persist Security Info=True;User ID=lts;Password=Abc@1234");
            SqlCommand cmd = new SqlCommand("USP_GetTemporaryPassword", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@useremail", email);

            try
            {
                con.Open();
            }
            catch (Exception)
            {
                //return "Error in opening Database";
            }
            SqlDataReader rdr=null;
            try { rdr = cmd.ExecuteReader(); } catch (Exception) { }
            
            while (rdr.Read())
            {
                temporarypassword = rdr["UserTempPassword"].ToString();
            }
            con.Close();
            if(temporarypassword==temppassword)
            {
                return true;
            }
            return false;
        }
        public string changePassword(string email,string password)
        { 
            string changepassword; 
            var hashed_password = RegisterController.hash(password, email);
            var connectionString = @"Data Source=dynamites.database.windows.net;Initial Catalog=LTS;Persist Security Info=True;User ID=LTS;Password=Abc@1234";
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand("USP_InsertNewPassword", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@useremail", email);
            cmd.Parameters.AddWithValue("@password", hashed_password);
            //cmd.Parameters.AddWithValue("@userphone", userinformation.PhoneNumber);
            try
            {
                con.Open();
            }
            catch (Exception)
            {
                //return "Error in opening Database";
            }
            var response = cmd.ExecuteNonQuery();
            con.Close();


            if (response != 0)
            {
                changepassword = email + " password has changed Successfully";
                return changepassword;
            }
            else
            {
                changepassword = "Password not changed";
                return changepassword; 
            }

        }

        public string Post(UserInformation userinformation)
        {
            // userinformation.UserEmail= getEmail(userinformation.tempPassword);
            bool validatetemppassword = checktemporarypassword(userinformation.tempPassword, userinformation.UserEmail);
            if (validatetemppassword)
            {
                var status = changePassword(userinformation.UserEmail,userinformation.UserPassword );
                  return status;
            }
            return "invalid temporary password";
            
        }
    }
}

