﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WBLT.Models
{
    public class UserInformation
    {

        public int UserID { get; set; }
        public string UserEmail { get; set; }//email id
        public string UserPassword { get; set; }//stored as hash
        public string PhoneNumber { get; set; }
        public bool isVerified { get; set; }
        public string tempPassword { get; set; }

    }
}