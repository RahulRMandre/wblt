﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WBLT.Models
{
    public class OtpSession
    {
        public string email { get; set; }
        public string otp { get; set; }
        public string sessionId { get; set; }
    }
}