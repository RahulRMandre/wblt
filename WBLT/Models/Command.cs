﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WBLT.Models
{
    public class Command
    {
        public string userEmail { get; set; }
        public string currentDirectory { get; set; }
        public string parameter1 { get; set; }
        public string parameter2 { get; set; }

    }
}